#!/bin/bash
set -eu

/etc/init.d/postgresql start

exec "$@"
