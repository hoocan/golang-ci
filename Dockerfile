FROM golang:1.7

# add postgres package
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
 && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - 

# install postgres
RUN apt-get update && apt-get install --no-install-recommends -y \
      postgresql-9.6 \
 && apt-get clean \
 && /etc/init.d/postgresql start \
 && su - -c "psql -c \"create user root superuser createdb createrole inherit login password ''\"" postgres \
 && /etc/init.d/postgresql stop

# install dumb-init
RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 \
 && chmod +x /usr/local/bin/dumb-init

# install swagger
RUN go get -v github.com/go-swagger/go-swagger/cmd/swagger

# init script
COPY start.sh /usr/local/bin/
ENTRYPOINT ["/usr/local/bin/dumb-init", "--", "/usr/local/bin/start.sh"]
